import 'babel-polyfill';
// import "core-js/stable"
// import "regenerator-runtime/runtime"

import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import axios from 'axios';

import App from './App.vue';

new Vue({
  el:'#app',
  render: h => h(App),
  data: {
    message: 'Hello!',
    isChecked: true,
    info: null
  },
  mounted () {
    axios
      .get('https://api.coindesk.com/v1/bpi/currentprice.json')
      .then(response => (this.info = response))
  }
});