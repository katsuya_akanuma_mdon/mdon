new Vue({
  el: '#app',
  data: {
    inputText: '',
    lists: [
      {
        name: 'ネギを買う',
        isDone: false
      },
      {
        name: 'ネギ1を買う',
        isDone: false
      }
    ]
  },
  methods: {
    onAdd () {
      if (this.inputText) {
        this.lists.push({
          name: this.inputText + 'minose',
          isDone: false,
        });
        this.inputText = '';
      }
    },
    onDelete (index) {
      this.lists.splice(index, 1);
    }
  }
})